﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddle : MonoBehaviour {

	private Rigidbody rigidbody;
	public float speed = 20.0f;
	public float maxSpeed = 20.0f;
	public float brake = 100.0f;
	public float acceleration = 10.0f;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;
	}

	void FixedUpdate () {
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal * speed * Time.deltaTime, 0, moveVertical * speed * Time.deltaTime);

		// move the object
		rigidbody.AddForce (movement * speed);

		if (moveHorizontal > 0) {
			speed = speed + acceleration * Time.deltaTime;
		} else if (moveHorizontal < 0 & moveHorizontal >= -maxSpeed) {
			speed = speed - acceleration * Time.deltaTime;
		} else {
			if (speed > 0) {
				speed = speed - (Mathf.Max (brake * Time.deltaTime, 0, Mathf.Abs (speed)));
			} else {
				speed = speed + (Mathf.Min (brake * Time.deltaTime, 0, Mathf.Abs (speed)));
			}
		}

		// clamp the speed
		speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);
		// clamp the brake
		brake = Mathf.Clamp (brake, 0, acceleration);

	}
} 